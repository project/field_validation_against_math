Field Validation Against Math
=============================

Ignacio Segura (nacho@isegura.es)

This module is a plugin for Field Validation module. The purpose of this module is to give site builders a method to set validation criteria that compare current field value to other values in one or more fields from the same form, combined, with no custom code involved. It allows simple comparisons and math operations.

How does it work:
-----------------

For example:

"Net income should be equal to gross income minus taxes."

Would be

field_net_income as source field, comparison set to "equal", and then we write:

field_gross_income - field_income_tax - field_some_other_tax

In the value field.

At the moment of writing this, it only works for numeric fields. It currently supports most usual logical operators: equal, not equal, greater than, greater or equal to, etc.
How operation is solved

Using a regex, all field names are found and replaced by their current values, or zero if they're blank. Then the math operation is executed and compared with base field's value.

Supporters
----------

Project was developed for SIASAR project (Rural Water and Sanitation Information System), with the support of World Bank Group, and it's used there on a daily basis.
Supporting organizations: 
Nik Nak Studio
Mentoring

